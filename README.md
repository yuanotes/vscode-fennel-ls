# fennel-ls

Visual Studio Code extension

Fennel language server integration

Connects Visual Studio Code to a [Language Server](https://en.wikipedia.org/wiki/Language_Server_Protocol) - nothing more and nothing less.

## Status

It should work, but might not. Tread carefully, and report back.

## Setup

A compatible Fennel language server is required.

Either set `fennel-lsx.server-version` to `"bundled"` to use the bundled version of the [fennel-ls](https://git.sr.ht/~xerool/fennel-ls/) language server.
Lua >= 5.1 is still required.

Or, set it to `"external"` to use the executable specified with `fennel-lsx.executable`.

## Fennel language support

This extension provides basic Fennel language support,
but does **not** provide any syntax hightlighting.
Use additional extensions:
e.g. [kongeor.vsc-fennel](https://github.com/kongeor/vsc-fennel),
or [adjuvant.fennel-syntax](https://codeberg.org/adjuvant/vscode-fennel-syntax) if you feel adventurous.

## Configuration

The configuration section `fennel-ls` mimics the [available options of fennel-ls](https://git.sr.ht/~xerool/fennel-ls/#default-settings).

## TODO

- Command to rebuild bundled executable
- Command to restart the language server
- Expand the capabilities of [the server](https://git.sr.ht/~xerool/fennel-ls/)
- Maybe [Semantic Highlighting](https://code.visualstudio.com/api/language-extensions/semantic-highlight-guide)?

## Development

Start the "Run Extension" task in vscode (F5)

The `npm run watch` default build task should start automatically in the background.

### Installation

```
npm install --ignore-scripts
npx vsce package
codium --install-extension vscode-fennel-ls-0.1.0.vsix
```

### How to update bundled fennel-ls

1. Update fennel-ls version in `nix/fennel-ls.nix`
1. Enter nix shell
1. Use command `bundle-fennel-ls` (shell.nix)
1. Increase version (and then npm i)
1. Add a changelog entry

### Notes

The configuration `<language-server-id>.trace.server` is used by the vscode-languageclient library to set its trace level.

## License

MIT License
