# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.0] - 2024-03-08

- Update fennel-ls to 0.1.2
  - New checks:
    - var-never-set
    - op-with-no-arguments
    - multival-in-middle-of-call
  - [Full changelog](https://git.sr.ht/~xerool/fennel-ls/tree/0.1.2/item/changelog.md)

## [0.4.4] - 2023-12-05

- Update fennel-ls:
  - Back to upstream, now natively includes: Configuration of extra allowed globals
  - New checks:
    - unnecessary-method: a call to the : builtin that could just be a multisym
    - bad-unpack: an unpack call leading into an operator
  - And more ...

## [0.4.3] - 2023-09-17

- Update fennel-ls:
  - Better completions for fields of tables
  - textDocument/rename Filter out when symbol references itself

## [0.4.2] - 2023-09-14

- Fix missing language-configuration.json file

## [0.4.1] - 2023-09-14

- No changes

## [0.4.0] - 2023-09-14

- Provide language configuration for Fennel.
  This is necessary to configure a word pattern for selection and symbol
  renaming.

## [0.3.0] - 2023-09-09

- Separate fennel-ls configuration from extension configuration `fennel-lsx`
  - Renamed: fennel-ls.executable -> fennel-lsx.executable
  - Renamed: fennel-ls.language-identifiers -> fennel-lsx.language-identifiers
- Make the selection between bundled or external language server explicit.
  A new config `fennel-lsx.server-version` is required.

## [0.2.0] - 2023-09-06

- (Optionally) Compile and use bundled fennel-ls

## [0.1.0] - 2023-09-05

- Initial release
