{ lib
, stdenv
, fetchgit
, lua
}:

# Fennel Language Server
# https://git.sr.ht/~xerool/fennel-ls

let
  version = "0.1.2";
  src = fetchgit {
    url = "https://git.sr.ht/~xerool/fennel-ls";
    rev = version;
    sha256 = "sha256-8TDJ03x9dkfievbovzMN3JRfIKba3CfzbcRAZOuPbKs=";
  };
in
stdenv.mkDerivation {
  inherit src version;
  pname = "fennel-ls";
  buildInputs = [
    lua
  ];
  dontConfigure = true;
  makeFlags = [ "PREFIX=$out" ];
  doCheck = false; # tests is broken in 0.1.2
}
